var usersList = document.querySelector('.users-list');
var firstName = document.querySelector('#first-name-select');
var xhr = new XMLHttpRequest();


function print(item) {
	resultHTML = `<li class="user">\
				  	<img class="user__avatar" src="${item.picture.medium}" alt="${item.name.title}_${item.name.first}_${item.name.last}">\
					<p class="user__name">${item.name.title}. ${item.name.first} ${item.name.last}</p>\
					<div class="user__info">\
						<div class="user__content clearfix">\
							<div class="user__close">X</div>\
							<img class="user__avatar-large" src="${item.picture.large}" alt="${item.name.title}_${item.name.first}_${item.name.last}">\
							<div class="user__description">\
								<div class="user__description-name">${item.name.title}. ${item.name.first} ${item.name.last}</div>\
								<p><span>Street: </span>${item.location.street}</p>\
								<p><span>City: </span>${item.location.city}</p>\
								<p><span>State: </span>${item.location.state}</p>\
								<p><span>Email: </span>${item.email}</p>\
								<p><span>Phone: </span>${item.phone}</p>\
							</div>
						</div>\
					</div>\
                  </li>`;
	
	usersList.innerHTML += resultHTML;
}
function sortIncrease(result) {
	function compare(a, b) {
		if (a.name.first[0] > b.name.first[0]) return 1;
		if (a.name.first[0] < b.name.first[0]) return -1;
	}
	result.sort(compare);
	return result;
}
function sortDecrease(result) {
	function compare(a, b) {
		if (a.name.first[0] < b.name.first[0]) return 1;
		if (a.name.first[0] > b.name.first[0]) return -1;
	}
	result.sort(compare);
	return result;
}
function sortNames(result) {
	var selInd = firstName.options.selectedIndex;
	
	if (firstName.options[selInd].value == 'increase') {
		sortIncrease(result);
	} else if (firstName.options[selInd].value == 'decrease') {
		sortDecrease(result);
	} else {
		return;
	}
	return result;
}
function camelize(str) {
  var arr = str.split(' ');
  for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }
  return arr.join(' ');
}
function printAll(result) {
	result.forEach(function(item, index) {
		item.name.title = camelize(item.name.title);
		item.name.first = camelize(item.name.first);
		item.name.last = camelize(item.name.last);
		item.location.street = camelize(item.location.street);
		item.location.city = camelize(item.location.city);
		item.location.state = camelize(item.location.state);
		
		print(item);
		
	});
}
function start() {
	var array = JSON.parse(xhr.responseText);
	var result = array['results'];
	result = sortNames(result);
	printAll(result);
}

firstName.addEventListener( "click", function(event) {
	event.preventDefault();
	usersList.innerHTML = '';
	start();
});

usersList.addEventListener("click", function(event) {
    var target = event.target;
	var	close = target.classList.contains("user__close");
    while (target != usersList) {
		if (target.classList.contains("user__content") && !close) break;
        if (target.classList.contains("user")) {
            target.querySelector(".user__info").classList.toggle("user__info--visible");
			break;
        }
        target = target.parentNode;
    }
});

function loadUsers() {
	xhr.open(
		'GET', 
		'https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture',
		true
	);
	xhr.send();
	xhr.onreadystatechange = function() {
		if (xhr.readyState != 4) return;

		if (xhr.status != 200) {
			alert(xhr.status + ': ' + xhr.statusText);
		} else {
			start();
		}
	}
}

loadUsers();
